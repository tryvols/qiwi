# qiwi

Before run the parser:
1. Install node js, if not exists at the computer (https://nodejs.org/)
2. Execute `npm i` at the root of the project in the command prompt
3. Open `cypress/integration/qiwi_spec.js` file and enter <i>user credentials</i>

To execute parser:
1. Execute `npm run start` at the root of the project in the command prompt

Result page will be located in the `page.html` file at the root of project