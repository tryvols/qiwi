
const delay = async (duration) => await new Promise((resolve) => { setTimeout(resolve, duration) });
const delayPreset = (duration = 1000) => async () => await delay(duration);

describe('Qiwi parser', () => {
    it('Start', () => {
        // Here you are writing your credentials
        const credentials = {
            login: '*****',
            password: '*****',
        };

        cy.visit('https://qiwi.com')
            // Wait until page load
            .then(delayPreset())
            .then(function() {
                // Open modal with authorization form
                cy.get('a').contains('Войти').click();

                // Input form data
                cy.get('input[type=tel]')
                    .clear()
                    .type(credentials.login);
                cy.get('input[type=password]')
                    .type(credentials.password);

                // Submit form
                cy.get('form').submit();
            })
            // Wait until page load
            .then(delayPreset(1500))
            .then(function() {
                // Find link contains Переводы and click it
                cy.get('a').contains('Переводы').click();
            })
            // Wait until page load
            .then(delayPreset(2000))
            .then(function() {
                // Write html of the page to the page.html file at the root of the project
                cy.document().then(data => cy.writeFile('page.html', `<html>${data.documentElement.innerHTML}</html>`));
            });
    })
})